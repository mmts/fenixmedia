<?php
//Loads css file
wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6' );	
wp_enqueue_style( 'bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array(), '3.3.6' );
wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css', array(), '1' );
wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/css/slick-theme.css', array(), '1' );

wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', array(), '1' );
wp_enqueue_style( 'arrows', get_template_directory_uri() . '/css/arrows.css', array(), '1' );

//Loads JavaScript file
wp_enqueue_script( 'bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '2016-01-18', false );
wp_enqueue_script( 'index', get_template_directory_uri() . '/js/index.js', array( 'jquery' ), '2015-01-18', false );
//wp_enqueue_script( 'Scrollify-master', get_template_directory_uri() . '/js/Scrollify-master/jquery.scrollify.js', array( 'jquery' ), '2015-01-18', false );
//wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.js', array( 'jquery' ), '2016-04-06', false );
//wp_enqueue_script( 'flowtype', get_template_directory_uri() . '/js/flowtype.js', array( 'jquery' ), '2015-02-16', false );
//wp_enqueue_script( 'classie', get_template_directory_uri() . '/js/classie.js');
//wp_enqueue_script( 'captcha', 'https://www.google.com/recaptcha/api.js');


/* Cargar Panel de Opciones
/*-----------------------------------------*/
if ( !function_exists( 'optionsframework_init' ) ) {
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
require_once dirname( __FILE__ ) . '/inc/options-framework.php';
}


function link_func( $atts ) {
    $a = shortcode_atts( array(
        'permalink' => '/',
    ), $atts );

    return  'href="'.site_url($a['permalink']).'"';
}
add_shortcode( 'link', 'link_func' );


add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
// Add support SVG files
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/*************************************************************
***************     Start Custom Type Home     ***************
**************************************************************/
function PostTypeHome() {
  $labels = array(
    'type'                  => 'home',
    'name'                  => 'Home',
    'singular_name'         => 'Home',
    'menu_name'             => 'Home',
    'name_admin_bar'        => 'Home',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All Home',
    'add_new_item'          => 'Add New Home',
    'add_new'               => 'Add New Home',
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit Home',
    'update_item'           => 'Update Home',
    'view_item'             => 'View Home',
    'search_items'          => 'Search Home',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );
  $args = array(
    'label'                 => 'Home',
    'description'           => 'Add Home',
    'labels'                => $labels,
    'supports'              => array( 'title','author','page-attributes',),
    'taxonomies'            => array( 'category', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-sos',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'post_home', $args );

}
add_action( 'init', 'PostTypeHome', 0 );


add_filter( 'rwmb_meta_boxes', 'MetaBoxesHome' );
function MetaBoxesHome( $meta_boxes )
{
  $prefix = 'home';
  $meta_boxes[] = array(
    'title'  => __( 'Header Content' ),
    'post_types' => 'post_home',
    'fields' => array(
      array(
        'id'          => 'home-descrition',
        'desc'        => __( 'Text ', 'your-prefix' ),
        'type'        => 'textarea',
        'clone'       => false,
        'placeholder' => __( 'Enter text', 'your-prefix' ),
        'rows'        => 5,
        'cols'        => 5,
      ),
//      array(
//        'id'   => 'bg-photo',
//        'name' => __( 'Home Bagkground Photo'),
//        'desc'  => 'Please, upload yours photos',
//        'type' => 'image_advanced',
//        'max_file_uploads' => 5
//      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'          => 'url-reef-title',
        'desc'        => __( 'REEF Title', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the REEF title', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'url-video-reef',
        'desc'        => __( 'URL Video REEF', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the Video URL', 'your-prefix' ),
        'size'        => 50,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'   => 'team-image',
        'name' => __( 'Team Images'),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
    ),
  );
  return $meta_boxes;
}
/**************************************************************
 ***************      End Custom Type Home      ***************
 **************************************************************/

/*************************************************************
 ***************     Start Custom Type Work     ***************
 **************************************************************/
function PostTypeWork() {
  $labels = array(
    'type'                  => 'work',
    'name'                  => 'Work',
    'singular_name'         => 'Work',
    'menu_name'             => 'Work',
    'name_admin_bar'        => 'Work',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All Works',
    'add_new_item'          => 'Add New Work',
    'add_new'               => 'Add New Work',
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit Work',
    'update_item'           => 'Update Work',
    'view_item'             => 'View Work',
    'search_items'          => 'Search Work',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );
  $args = array(
    'label'                 => 'Work',
    'description'           => 'Add Work',
    'labels'                => $labels,
    'supports'              => array( 'title','author','page-attributes',),
    'taxonomies'            => array( 'category', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-sos',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'post_work', $args );

}
add_action( 'init', 'PostTypeWork', 0 );


add_filter( 'rwmb_meta_boxes', 'MetaBoxesWork' );
function MetaBoxesWork( $meta_boxes )
{
  $prefix = 'work';
  $meta_boxes[] = array(
    'title'  => __( 'Header Content' ),
    'post_types' => 'post_work',
    'fields' => array(
      array(
        'id'          => 'client-name-work',
        'desc'        => __( 'Client Name', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter Client Name', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'client-campaign-work',
        'desc'        => __( 'Client Campaign Name', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter Campaign Name', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'client-desc',
        'desc'        => __( 'Description ', 'your-prefix' ),
        'type'        => 'textarea',
        'clone'       => false,
        'placeholder' => __( 'Enter Description', 'your-prefix' ),
        'rows'        => 5,
        'cols'        => 5,
      ),
      array(
        'id'   => 'icon-work',
        'name' => __( 'Capture Photo'),
        'desc'  => 'Please, upload your photo',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'          => 'url-video-work',
        'desc'        => __( 'URL Video', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the Video URL', 'your-prefix' ),
        'size'        => 50,
      ),
    ),
  );
  return $meta_boxes;
}
/**************************************************************
 ***************      End Custom Type Work      ***************
 **************************************************************/

/**************************************************************
 ***************    Start Custom Type Facts     ***************
 **************************************************************/
function PostTypeFacts() {
  $labels = array(
    'type'                  => 'facts',
    'name'                  => 'Fact',
    'singular_name'         => 'Facts',
    'menu_name'             => 'Facts',
    'name_admin_bar'        => 'Facts',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All Facts',
    'add_new_item'          => 'Add New Fact',
    'add_new'               => 'Add New Fact',
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit Fact',
    'update_item'           => 'Update Fact',
    'view_item'             => 'View Fact',
    'search_items'          => 'Search Fact',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );
  $args = array(
    'label'                 => 'Fact',
    'description'           => 'Add Fact',
    'labels'                => $labels,
    'supports'              => array( 'title','author','page-attributes',),
    'taxonomies'            => array( 'category', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-sos',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'post_facts', $args );

}
add_action( 'init', 'PostTypeFacts', 0 );


add_filter( 'rwmb_meta_boxes', 'MetaBoxesFacts' );
function MetaBoxesFacts( $meta_boxes )
{
  $prefix = 'facts';
  $meta_boxes[] = array(
    'title'  => __( 'Header Content' ),
    'post_types' => 'post_facts',
    'fields' => array(
      array(
        'id'   => 'facts-icon-one',
        'name' => __( 'Icon' ),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'          => 'facts-descrition-one',
        'desc'        => __( 'Description ', 'your-prefix' ),
        'type'        => 'textarea',
        'clone'       => false,
        'placeholder' => __( 'Enter Description', 'your-prefix' ),
        'rows'        => 5,
        'cols'        => 5,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'   => 'facts-icon-two',
        'name' => __( 'Icon' ),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'          => 'facts-descrition-two',
        'desc'        => __( 'Description ', 'your-prefix' ),
        'type'        => 'textarea',
        'clone'       => false,
        'placeholder' => __( 'Enter Description', 'your-prefix' ),
        'rows'        => 5,
        'cols'        => 5,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'   => 'facts-icon-percent-one',
        'name' => __( 'Icon' ),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'          => 'facts-percentage-desc-one',
        'desc'        => __( 'Fact Percentage Description', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the fact percentage description', 'your-prefix' ),
        'size'        => 50,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'   => 'facts-icon-percent-two',
        'name' => __( 'Icon' ),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'          => 'facts-percentage-desc-two',
        'desc'        => __( 'Fact Percentage Description', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the fact percentage description', 'your-prefix' ),
        'size'        => 50,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'   => 'facts-icon-percent-three',
        'name' => __( 'Icon' ),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'          => 'facts-percentage-desc-three',
        'desc'        => __( 'Fact Percentage Description', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the fact percentage description', 'your-prefix' ),
        'size'        => 50,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'   => 'facts-icon-percent-four',
        'name' => __( 'Icon' ),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'          => 'facts-percentage-desc-four',
        'desc'        => __( 'Fact Percentage Description', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the fact percentage description', 'your-prefix' ),
        'size'        => 50,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'   => 'facts-icon-percent-five',
        'name' => __( 'Icon' ),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'          => 'facts-percentage-desc-five',
        'desc'        => __( 'Fact Description', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the fact description', 'your-prefix' ),
        'size'        => 50,
      ),
    ),
  );
  return $meta_boxes;
}
/**************************************************************
 ***************     End Custom Type Facts      ***************
 **************************************************************/

/*************************************************************
 ***************   Start Custom Type Service   ***************
 **************************************************************/
function PostTypeService() {
  $labels = array(
    'type'                  => 'service',
    'name'                  => 'Service',
    'singular_name'         => 'Service',
    'menu_name'             => 'Services',
    'name_admin_bar'        => 'Service',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All Services',
    'add_new_item'          => 'Add New Service',
    'add_new'               => 'Add New Service',
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit Service',
    'update_item'           => 'Update Service',
    'view_item'             => 'View Service',
    'search_items'          => 'Search Service',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );
  $args = array(
    'label'                 => 'Services',
    'description'           => 'Add Service',
    'labels'                => $labels,
    'supports'              => array( 'title','author','page-attributes',),
    'taxonomies'            => array( 'category', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-sos',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'post_service', $args );

}
add_action( 'init', 'PostTypeService', 0 );


add_filter( 'rwmb_meta_boxes', 'MetaBoxesService' );
function MetaBoxesService( $meta_boxes )
{
  $prefix = 'service';
  $meta_boxes[] = array(
    'title'  => __( 'Header Content' ),
    'post_types' => 'post_service',
    'fields' => array(
      array(
        'id'   => 'bg-images',
        'name' => __( 'Service Bagkground Image'),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'   => 'bg-images-mob',
        'name' => __( 'Service Bagkground Image for Mobile'),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'   => 'bg-images-msg',
        'name' => __( 'Service Message Mobile'),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'   => 'bg-images-msg1',
        'name' => __( 'Service Message Mobile'),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
    ),
  );
  return $meta_boxes;
}
/**************************************************************
 ***************    End Custom Type Service     ***************
 **************************************************************/

/**************************************************************
 ***************    Start Custom Type Team      ***************
 **************************************************************/
function PostTypeTeam() {
  $labels = array(
    'type'                  => 'team',
    'name'                  => 'Team Members',
    'singular_name'         => 'Team Members',
    'menu_name'             => 'Team Members',
    'name_admin_bar'        => 'Team Members',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All Team Members',
    'add_new_item'          => 'Add New Team Member',
    'add_new'               => 'Add New Team Member',
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit Team Member',
    'update_item'           => 'Update Team Member',
    'view_item'             => 'View Team Member',
    'search_items'          => 'Search Team Member',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );
  $args = array(
    'label'                 => 'Team Members',
    'description'           => 'Add Team Member',
    'labels'                => $labels,
    'supports'              => array( 'title','author','page-attributes',),
    'taxonomies'            => array( 'category', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-sos',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'post_team', $args );

}
add_action( 'init', 'PostTypeTeam', 0 );


add_filter( 'rwmb_meta_boxes', 'MetaBoxesTeam' );
function MetaBoxesTeam( $meta_boxes )
{
  $prefix = 'team';
  $meta_boxes[] = array(
    'title'  => __( 'Header Content' ),
    'post_types' => 'post_team',
    'fields' => array(
      array(
        'id'   => 'team-photo',
        'name' => __( 'Team Member Photo' ),
        'desc'  => 'Please, upload your photo',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
      array(
        'id'          => 'team-member-name',
        'desc'        => __( 'Team Member Name', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the team member name', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'team-member-position',
        'desc'        => __( 'Team Member Position', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the team member position', 'your-prefix' ),
        'size'        => 50,
      ),
    ),
  );
  return $meta_boxes;
}
/**************************************************************
 ***************      End Custom Type Team      ***************
 **************************************************************/

/*************************************************************
 ***************   Start Custom Type Clients   ***************
 **************************************************************/
function PostTypeClient() {
  $labels = array(
    'type'                  => 'client',
    'name'                  => 'Clients',
    'singular_name'         => 'Client',
    'menu_name'             => 'Clients',
    'name_admin_bar'        => 'Clients',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All Clients',
    'add_new_item'          => 'Add New Client',
    'add_new'               => 'Add New Client',
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit Client',
    'update_item'           => 'Update Client',
    'view_item'             => 'View Client',
    'search_items'          => 'Search Client',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );
  $args = array(
    'label'                 => 'Clients',
    'description'           => 'Add Client',
    'labels'                => $labels,
    'supports'              => array( 'title','author','page-attributes',),
    'taxonomies'            => array( 'category', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-sos',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'post_client', $args );

}
add_action( 'init', 'PostTypeClient', 0 );


add_filter( 'rwmb_meta_boxes', 'MetaBoxesClients' );
function MetaBoxesClients( $meta_boxes )
{
  $prefix = 'client';
  $meta_boxes[] = array(
    'title'  => __( 'Header Content' ),
    'post_types' => 'post_client',
    'fields' => array(
      array(
        'id'   => 'client-logo',
        'name' => __( 'Client logo'),
        'desc'  => 'Please, upload your image',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
    ),
  );
  return $meta_boxes;
}
/**************************************************************
 ***************    End Custom Type Clients     ***************
 **************************************************************/

/*************************************************************
 ***************   Start Custom Type Stories   ***************
 **************************************************************/
function PostTypeStories() {
  $labels = array(
    'type'                  => 'stories',
    'name'                  => 'Stories',
    'singular_name'         => 'Story',
    'menu_name'             => 'Story',
    'name_admin_bar'        => 'Stories',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All Stories',
    'add_new_item'          => 'Add New Story',
    'add_new'               => 'Add New Story',
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit Story',
    'update_item'           => 'Update Story',
    'view_item'             => 'View Story',
    'search_items'          => 'Search Story',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );
  $args = array(
    'label'                 => 'Stories',
    'description'           => 'Add Story',
    'labels'                => $labels,
    'supports'              => array( 'title','author','page-attributes',),
    'taxonomies'            => array( 'category', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-sos',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'post_stories', $args );

}
add_action( 'init', 'PostTypeStories', 0 );


add_filter( 'rwmb_meta_boxes', 'MetaBoxesStories' );
function MetaBoxesStories( $meta_boxes )
{
  $prefix = 'stories';
  $meta_boxes[] = array(
    'title'  => __( 'Header Content' ),
    'post_types' => 'post_stories',
    'fields' => array(
      array(
        'id'          => 'stories-title',
        'desc'        => __( 'Story title', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the title story', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'stories-desc',
        'desc'        => __( 'Story description', 'your-prefix' ),
        'type'        => 'textarea',
        'clone'       => false,
        'placeholder' => __( 'Enter the description story', 'your-prefix' ),
        'rows'        => 5,
        'cols'        => 5,
      ),
      array(
        'id'          => 'stories-url',
        'desc'        => __( 'Story video', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the url video story', 'your-prefix' ),
        'size'        => 50,
      ),
    ),
  );
  return $meta_boxes;
}
/**************************************************************
 ***************    End Custom Type Stories     ***************
 **************************************************************/

/*************************************************************
 ***************   Start Custom Type Contact   ***************
 **************************************************************/
function PostTypeContact() {
  $labels = array(
    'type'                  => 'contact',
    'name'                  => 'Contact',
    'singular_name'         => 'Contact',
    'menu_name'             => 'Contact',
    'name_admin_bar'        => 'Contact',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All Stories',
    'add_new_item'          => 'Add New Contact',
    'add_new'               => 'Add New Contact',
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit Contact',
    'update_item'           => 'Update Contact',
    'view_item'             => 'View Contact',
    'search_items'          => 'Search Contact',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );
  $args = array(
    'label'                 => 'Contact',
    'description'           => 'Add Contact',
    'labels'                => $labels,
    'supports'              => array( 'title','author','page-attributes',),
    'taxonomies'            => array( 'category', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-sos',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'post_contact', $args );

}
add_action( 'init', 'PostTypeContact', 0 );


add_filter( 'rwmb_meta_boxes', 'MetaBoxesContact' );
function MetaBoxesContact( $meta_boxes )
{
  $prefix = 'contact';
  $meta_boxes[] = array(
    'title'  => __( 'Header Content' ),
    'post_types' => 'post_contact',
    'fields' => array(
      array(
        'id'          => 'contact-message',
        'desc'        => __( 'Contact Message', 'your-prefix' ),
        'type'        => 'textarea',
        'clone'       => false,
        'placeholder' => __( 'Enter the title story', 'your-prefix' ),
        'rows'        => 5,
        'cols'        => 5,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'          => 'contact-name-miami',
        'desc'        => __( 'Contact name (Miami)', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the contact name (Miami)', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'contact-phone-miami',
        'desc'        => __( 'Phone number (Miami)', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the phone number (Miami)', 'your-prefix' ),
        'size'        => 50,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'          => 'contact-name-ccs',
        'desc'        => __( 'Contact name (Caracas)', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the contact name (Caracas)', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'contact-phone-ccs',
        'desc'        => __( 'Phone number (Caracas)', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the phone number (Caracas)', 'your-prefix' ),
        'size'        => 50,
      ),
      array('name' => 'devider',
        'id' => $prefix.'',
        'type' => 'divider',
      ),
      array(
        'id'          => 'contact-fb',
        'desc'        => __( 'Facebook URL', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the facebook URL', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'contact-tw',
        'desc'        => __( 'Twitter URL', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the twitter URL', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'contact-ins',
        'desc'        => __( 'Instagram URL', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the instagram URL', 'your-prefix' ),
        'size'        => 50,
      ),
    ),
  );
  return $meta_boxes;
}
/**************************************************************
 ***************    End Custom Type Contact     ***************
 **************************************************************/