var $ = jQuery.noConflict();

$(document).ready(function(){

    $('#nv-ho').click(function(){
      $('html,body').animate({
          scrollTop: 0},
        'slow');
    });
    $('#nv-te').click(function(){
      $('#nv-te').addClass('active');
      $('#nv-wo').removeClass('active');
      $('#nv-st').removeClass('active');
      $('#nv-co').removeClass('active');
      $('html,body').animate({
          scrollTop: $("#team").offset().top},
        'slow');
    });
    $('#nv-wo').click(function(){
      $('#nv-wo').addClass('active');
      $('#nv-te').removeClass('active');
      $('#nv-st').removeClass('active');
      $('#nv-co').removeClass('active');
      $('html,body').animate({
          scrollTop: $("#work").offset().top},
        'slow');
    });
    $('#nv-st').click(function(){
      $('#nv-st').addClass('active');
      $('#nv-te').removeClass('active');
      $('#nv-wo').removeClass('active');
      $('#nv-co').removeClass('active');
      $('html,body').animate({
          scrollTop: $("#stories").offset().top},
        'slow');
    });
    $('#nv-co').click(function(){
      $('#nv-co').addClass('active');
      $('#nv-te').removeClass('active');
      $('#nv-wo').removeClass('active');
      $('#nv-st').removeClass('active');
      $('html,body').animate({
          scrollTop: $("#contacto").offset().top},
        'slow');
    });

    // Team Section
    $('#btn-fac').click(function(){
      $('#btn-fac').addClass('active');
      $('#btn-ser').removeClass('active');
      $('#btn-tea').removeClass('active');
      $('#teamFacts').addClass('active');
      $('#teamTeam').removeClass('active');
      $('#teamService').removeClass('active');
    });
    $('#btn-ser').click(function(){
      $('#btn-ser').addClass('active');
      $('#btn-fac').removeClass('active');
      $('#btn-tea').removeClass('active');
      $('#teamTeam').removeClass('active');
      $('#teamService').addClass('active');
      $('#teamFacts').removeClass('active');
    });
    $('#btn-tea').click(function(){
      $('#btn-tea').addClass('active');
      $('#btn-fac').removeClass('active');
      $('#btn-ser').removeClass('active');
      $('#teamTeam').addClass('active');
      $('#teamFacts').removeClass('active');
      $('#teamService').removeClass('active');
    });

    // Stories Section
    $('#btn-cli').click(function(){
      $('#btn-cli').addClass('active');
      $('#btn-sto').removeClass('active');
      $('#storiesCli').addClass('active');
      $('#storiesSto').removeClass('active');
      $('#stories').addClass('active');
    });
    $('#btn-sto').click(function(){
      $('#btn-sto').addClass('active');
      $('#btn-cli').removeClass('active');
      $('#storiesCli').removeClass('active');
      $('#storiesSto').addClass('active');
      $('#stories').removeClass('active');
    });
})
