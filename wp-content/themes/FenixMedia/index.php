<?php get_header();?>

<!-- Features Section -->
<div class="bg-gray">
  <section id="Home" class="col-xs-12 no-padding">
    <?php
      $args = array(
        'post_type' => 'post_home',
        'post_status' => 'publish',
        'orderby' => 'ID',
        'order' => 'ASC',
        'posts_per_page' => '1',
      );

      query_posts($args);
      if (have_posts()) :
        while (have_posts()) : the_post();
          $homeText = rwmb_meta('home-descrition',$post->ID );
          $reefTitle = rwmb_meta('url-reef-title',$post->ID );
          $reefUri = rwmb_meta('url-video-reef',$post->ID );
          $teamImg = rwmb_meta('team-image',array('type'=>'image'),$post->ID );
    ?>
      <div class="col-xs-12 fm-home">
        <?php
          if ($homeText) {
            echo "<p class='col-xs-12 text-center text-home center-div'>$homeText</p>";
          }
        ?>
      </div>
    <?php
      endwhile;
        else: ?>
      <p class="text-center"><?php _e('No hay Datos Cargados.'); ?></p>
    <?php endif;?>
  </section>

  <!--  Team  -->
  <section id="team" class="col-xs-12 no-padding">
    <div class="col-xs-12">
      <div class="container">
        <div class="row team-ppal">
          <div class="col-xs-12 team-buttons text-center">
            <?php
              if ($lang == "en") {
                echo '<a id="btn-ser" class="white-buttons active">Services</a>';
                echo '<a id="btn-fac" class="white-buttons">Facts</a>';
                echo '<a id="btn-tea" class="white-buttons">Team</a>';
              } else {
                echo '<a id="btn-ser" class="white-buttons active">Servicios</a>';
                echo '<a id="btn-fac" class="white-buttons">Datos</a>';
                echo '<a id="btn-tea" class="white-buttons">Equipo</a>';
              }
            ?>
          </div>

          <?php
            $args = array(
              'post_type' => 'post_service',
              'post_status' => 'publish',
              'orderby' => 'ID',
              'order' => 'ASC',
              'posts_per_page' => '1',
            );

            query_posts($args);
            if (have_posts()) :
              while (have_posts()) : the_post();
                $bgImage  = rwmb_meta('bg-images', array('type'=>'image'),$post->ID);
                $bgImageMob  = rwmb_meta('bg-images-mob', array('type'=>'image'),$post->ID);
                $bgImageMsg  = rwmb_meta('bg-images-msg', array('type'=>'image'),$post->ID);
                $bgImageMsg1  = rwmb_meta('bg-images-msg1', array('type'=>'image'),$post->ID);
              endwhile;
            endif;
          ?>
          <div id="teamService" class="div col-xs-12 no-padding active">
            <?php
                foreach ( $bgImage as $image ) {
                  echo "<img class='img-responsive center-block hidden-xs visible-sm visible-md visible-lg' src='{$image['url']}' alt='{$image['name']}' />";
                }
            ?>
            <div class="col-xs-12 no-padding visible-xs hidden-sm hidden-md hidden-lg img-padding-bt">
              <?php
                foreach ( $bgImageMob as $image ) {
                  echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                }
              ?>
            </div>
            <div class="col-xs-6 no-padding visible-xs hidden-sm hidden-md hidden-lg">
              <?php
                foreach ( $bgImageMsg as $image ) {
                  echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                }
              ?>
            </div>
            <div class="col-xs-6 no-padding visible-xs hidden-sm hidden-md hidden-lg">
              <?php
                foreach ( $bgImageMsg1 as $image ) {
                  echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                }
              ?>
            </div>
          </div>

          <?php
            $args = array(
              'post_type' => 'post_facts',
              'post_status' => 'publish',
              'orderby' => 'ID',
              'order' => 'ASC',
              'posts_per_page' => '1',
            );

            query_posts($args);
            if (have_posts()) :
              while (have_posts()) : the_post();
                $leftDesc = rwmb_meta('facts-descrition-one',$post->ID );
                $rightDesc = rwmb_meta('facts-descrition-two',$post->ID );
                $leftImage  = rwmb_meta('facts-icon-one', array('type'=>'image'),$post->ID);
                $rightImage  = rwmb_meta('facts-icon-two', array('type'=>'image'),$post->ID);
                $iconText1  = rwmb_meta('facts-percentage-desc-one', $post->ID);
                $iconText2  = rwmb_meta('facts-percentage-desc-two', $post->ID);
                $iconText3  = rwmb_meta('facts-percentage-desc-three', $post->ID);
                $iconText4  = rwmb_meta('facts-percentage-desc-four', $post->ID);
                $iconText5  = rwmb_meta('facts-percentage-desc-five', $post->ID);
                $iconImage1  = rwmb_meta('facts-icon-percent-one', array('type'=>'image'),$post->ID);
                $iconImage2  = rwmb_meta('facts-icon-percent-two', array('type'=>'image'),$post->ID);
                $iconImage3  = rwmb_meta('facts-icon-percent-three', array('type'=>'image'),$post->ID);
                $iconImage4  = rwmb_meta('facts-icon-percent-four', array('type'=>'image'),$post->ID);
                $iconImage5  = rwmb_meta('facts-icon-percent-five', array('type'=>'image'),$post->ID);

              endwhile;
            endif;
          ?>

          <div id="teamFacts" class="col-xs-12 no-padding">
            <div id="CarouselFactsDesc" class="carousel slide visible-xs hidden-sm hidden-md hidden-lg" data-ride="carousel">
              <!--  Carousel Indicators-->
              <ol class="carousel-indicators fm-carousel-indicators">
                  <li data-target="#CarouselFactsDesc" data-slide-to="0" class="active"></li>
                  <li data-target="#CarouselFactsDesc" data-slide-to="1"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <?php
                    foreach ( $leftImage as $image ) {
                      echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                    }
                  ?>
                  <p class="text-facts-desc text-center"><?php echo $leftDesc; ?></p>
                </div>
                <div class="item">
                  <?php
                    foreach ( $rightImage as $image ) {
                      echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                    }
                  ?>
                  <p class="text-facts-desc text-center"><?php echo $rightDesc; ?></p>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding div-right-border hidden-xs visible-sm visible-md visible-lg">
              <?php
                foreach ( $leftImage as $image ) {
                  echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                }
              ?>
              <p class="text-facts-desc text-center"><?php echo $leftDesc; ?></p>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding hidden-xs visible-sm visible-md visible-lg">
              <?php
                foreach ( $rightImage as $image ) {
                  echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                }
              ?>
              <p class="text-facts-desc text-center"><?php echo $rightDesc; ?></p>
            </div>

            <div class="col-xs-12 no-padding mt-30">
            <div class="col-xs-12 col-sm-2 col-xs-offset-0 col-sm-offset-1 no-padding hidden-xs visible-sm visible-md visible-lg">
              <?php
              foreach ( $iconImage1 as $image ) {
                echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
              }
              ?>
              <p class="text-facts-icons text-center"><?php echo $iconText1; ?></p>
            </div>
            <div class="col-xs-12 col-sm-2 no-padding hidden-xs visible-sm visible-md visible-lg">
              <?php
              foreach ( $iconImage2 as $image ) {
                echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
              }
              ?>
              <p class="text-facts-icons text-center"><?php echo $iconText2; ?></p>
            </div>
            <div class="col-xs-12 col-sm-2 no-padding hidden-xs visible-sm visible-md visible-lg">
              <?php
              foreach ( $iconImage3 as $image ) {
                echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
              }
              ?>
              <p class="text-facts-icons text-center"><?php echo $iconText3; ?></p>
            </div>
            <div class="col-xs-12 col-sm-2 no-padding hidden-xs visible-sm visible-md visible-lg">
              <?php
              foreach ( $iconImage4 as $image ) {
                echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
              }
              ?>
              <p class="text-facts-icons text-center"><?php echo $iconText4; ?></p>
            </div>
            <div class="col-xs-12 col-sm-2 no-padding hidden-xs visible-sm visible-md visible-lg">
              <?php
                foreach ( $iconImage5 as $image ) {
                  echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                }
              ?>
              <p class="text-facts-icons text-center"><?php echo $iconText5; ?></p>
            </div>

            <div id="CarouselFactsImgs" class="carousel slide visible-xs hidden-sm hidden-md hidden-lg" data-ride="carousel">
              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <?php
                    foreach ( $iconImage1 as $image ) {
                      echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                    }
                  ?>
                  <p class="text-facts-icons text-center"><?php echo $iconText1; ?></p>
                </div>
                <div class="item">
                  <?php
                    foreach ( $iconImage2 as $image ) {
                      echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                    }
                  ?>
                  <p class="text-facts-icons text-center"><?php echo $iconText2; ?></p>
                </div>
                <div class="item">
                  <?php
                    foreach ( $iconImage3 as $image ) {
                      echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                    }
                  ?>
                  <p class="text-facts-icons text-center"><?php echo $iconText3; ?></p>
                </div>
                <div class="item">
                  <?php
                    foreach ( $iconImage4 as $image ) {
                      echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                    }
                  ?>
                  <p class="text-facts-icons text-center"><?php echo $iconText4; ?></p>
                </div>
                <div class="item">
                  <?php
                    foreach ( $iconImage5 as $image ) {
                      echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                    }
                  ?>
                  <p class="text-facts-icons text-center"><?php echo $iconText5; ?></p>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#CarouselFactsImgs" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#CarouselFactsImgs" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>

            </div>
          </div>

          </div>

          <div id="teamTeam" class="div col-xs-12 no-padding">
            <?php
              $args = array(
                'post_type' => 'post_team',
                'post_status' => 'publish',
                'orderby' => 'ID',
                'order' => 'ASC',
                'posts_per_page' => '-1',
              );

              query_posts($args);
              if (have_posts()) :
                while (have_posts()) : the_post();
                  $teamPhoto = rwmb_meta('team-photo',array('type'=>'image'),$post->ID );
                  $teamName = rwmb_meta('team-member-name',$post->ID );
                  $teamPos = rwmb_meta('team-member-position',$post->ID );
            ?>
                  <div class="col-xs-6 col-sm-4">
                    <?php
                      foreach ( $teamPhoto as $image ) {
                        echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                      }
                    ?>
                    <p class="team-name"><?php echo $teamName; ?></p>
                    <p class="team-desc"><?php echo $teamPos; ?></p>
                  </div>
            <?php
                endwhile;
              endif;
            ?>
            <div class="col-xs-12 col-xs-4 hidden-xs visible-sm visible-md visible-lg">
              <?php
                foreach ( $teamImg as $image ) {
                  echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                }
              ?>
            </div>
            <div class="col-xs-6 visible-xs hidden-sm hidden-md hidden-lg no-padding"></div>
            <div class="col-xs-12 visible-xs hidden-sm hidden-md hidden-lg no-padding">
            <?php
                foreach ( $teamImg as $image ) {
                  echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                }
              ?>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <!--  Work  -->
  <section id="work" class="col-xs-12 no-padding">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 work-ppal nopadding">
          <?php
            $args = array(
              'post_type' => 'post_work',
              'post_status' => 'publish',
              'orderby' => 'ID',
              'order' => 'ASC',
              'posts_per_page' => '-1',
            );

            query_posts($args);

            $modalCoun = 1;
            $countWork = $wp_query->post_count;
            $countWorkOne = $wp_query->post_count;
            if (have_posts()) :
              while (have_posts()) : the_post();
                $clientIcon  = rwmb_meta('icon-work', array('type'=>'image'),$post->ID);
                $clientName = rwmb_meta('client-name-work',$post->ID );
                $clientDesc = rwmb_meta('client-desc',$post->ID );
                $clientCampaign = rwmb_meta('client-campaign-work',$post->ID );
                $clientVideoUri  = rwmb_meta('url-video-work', $post->ID);
            ?>
              <div class="col-xs-6 col-sm-3 block-work no-padding hidden-xs visible-sm visible-md visible-lg">
                <?php
                  foreach ( $clientIcon as $image ) {
                    echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                  }
                ?>
                <p class="text-work center-block" data-toggle="modal" data-target="<?php echo "#modal".$modalCoun; ?>"><?php echo $clientName; ?> / <?php echo $clientCampaign; ?></p>
                <p class="text-work center-block" data-toggle="modal" data-target="<?php echo "#modal".$modalCoun; ?>"><?php echo $clientDesc; ?></p>
              </div>

            <?php
              $modalCoun++;
              endwhile;
            endif;

            $countWork = $wp_query->post_count;
            $countWorkOne = $wp_query->post_count;

          ?>
          <!--     Carousel Mobile       -->
          <div id="workCarousel" class="carousel slide visible-xs hidden-sm hidden-md hidden-lg" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <?php
                $first = 0;
                $contInd = 0;
                $countPost = 1;
                while ($countWork > 0):
                  if ($countPost % 6 == 0):
                    echo 'e';
                    if ($first == 0):
                      $first = 1;
                      echo '<li data-target="#workCarousel" data-slide-to="'.$contInd.'" class="active"></li>';
                    else:
                      echo '<li data-target="#workCarousel" data-slide-to="'.$contInd.'"></li>';
                    endif;
                  else:
                    if ($countPost == ($wp_query->post_count)):
                      echo '<li data-target="#workCarousel" data-slide-to="'.$contInd.'"></li>';
                    endif;
                  endif;
                  $contInd++;
                  $countPost++;
                  $countWork--;
                endwhile;
                $countPost = 0;
                $countWork = $wp_query->post_count;
              ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <?php
                $modalCoun = 1;
                if (have_posts()) :
                  while (have_posts()) : the_post();
                    $clientIcon  = rwmb_meta('icon-work', array('type'=>'image'),$post->ID);
                    $clientName = rwmb_meta('client-name-work',$post->ID );
                    $clientDesc = rwmb_meta('client-desc',$post->ID );
                    $clientCampaign = rwmb_meta('client-campaign-work',$post->ID );
                    $clientVideoUri  = rwmb_meta('url-video-work', $post->ID);
                    if ($countWork == $countWorkOne):
                      echo '<div class="item active">';
                    endif;
                  ?>
                    <div class="col-xs-6 block-work">
                      <?php
                        foreach ( $clientIcon as $image ) {
                          echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                        }
                      ?>
                      <p class="text-work center-block" data-toggle="modal" data-target="<?php echo "#modal".$modalCoun; ?>"><?php echo $clientName; ?> / <?php echo $clientCampaign; ?></p>
                      <p class="text-work center-block" data-toggle="modal" data-target="<?php echo "#modal".$modalCoun; ?>"><?php echo $clientDesc; ?></p>
                    </div>
                <?php
                    $countPost++;
                    $countWork--;
                    if ($countPost % 6 == 0):
                      echo '</div>';
                      echo '<div class="item">';
                    else:
                      if ($countPost == ($wp_query->post_count)):
                        echo '</div>';
                      endif;
                    endif;
                    $modalCoun++;
                  endwhile;
                endif;
              ?>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#workCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#workCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>


          <!--     Work Details     -->
          <?php
            $countID = 1;
            if (have_posts()) :
              while (have_posts()) : the_post();
                $clientVideoUri  = rwmb_meta('url-video-work', $post->ID);
            ?>
                <div class="modal fade" id="modal<?php echo $countID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-body">
                        <button type="button" class="close col-xs-12 pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <iframe width="720" height="480" src="<?php echo $clientVideoUri; ?>" frameborder="0" allowfullscreen="" class="center-block col-xa-12"></iframe>
                      </div>
                    </div>
                  </div>
                </div>
            <?php
                $countID++;
              endwhile;
            endif;
          ?>

        </div>
      </div>
    </div>
  </section>

  <!--  REEF  -->
  <section id="reef" class="col-xs-12 no-padding">
    <div class="col-xs-12 stories-ppal center-div">
      <div class="container">
        <p class="text-center reef-title"><?php echo $reefTitle; ?></p>
        <iframe width="720" height="480" src="<?php echo $reefUri; ?>" frameborder="0" allowfullscreen="" class="center-block"></iframe>
      </div>
    </div>
  </section>

  <!--  Client/Stories  -->
  <section id="stories" class="col-xs-12 no-padding">
    <div class="col-xs-12 stories-ppal center-div">
      <div class="container">
        <div class="col-xs-12 team-buttons text-center">
          <?php
              if ($lang == "en") {
                echo '<a id="btn-cli" class="white-buttons">Clients</a>';
                echo '<a id="btn-sto" class="white-buttons active">Stories</a>';
              } else {
                echo '<a id="btn-cli" class="white-buttons">Clientes</a>';
                echo '<a id="btn-sto" class="white-buttons active">Historias</a>';
              }
            ?>


        </div>

        <div id="storiesCli" class="col-xs-12 no-padding">
          <?php
            $args = array(
              'post_type' => 'post_client',
              'post_status' => 'publish',
              'orderby' => 'ID',
              'order' => 'ASC',
              'posts_per_page' => '-1',
            );

            query_posts($args);
            if (have_posts()) :
              while (have_posts()) : the_post();
                $brand = rwmb_meta('client-logo',array('type'=>'image'),$post->ID );
                foreach ( $brand as $image ) {
                  echo "<div class='col-xs-4 col-sm-4 brand-div'>";
                  echo "<img class='img-responsive center-block' src='{$image['url']}' alt='{$image['name']}' />";
                  echo "</div>";
                }
              endwhile;
            endif;
          ?>
        </div>

        <div id="storiesSto" class="col-xs-12 no-padding active">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <?php
              $args = array(
                'post_type' => 'post_stories',
                'post_status' => 'publish',
                'orderby' => 'ID',
                'order' => 'ASC',
                'posts_per_page' => '10',
              );

              query_posts($args);
              $count = $wp_query->post_count;
              $countOne = $wp_query->post_count;
              ?>

            <!-- Indicators -->
            <ol class="carousel-indicators fm-carousel-indicators">
            <?php
              while ($count > 0):
                if ($count == $wp_query->post_count) :
                  echo '<li data-target="#myCarousel" data-slide-to="0" class="active"></li>';
                else:
                  echo '<li data-target="#myCarousel" data-slide-to="1"></li>';
                endif;

                $count--;
              endwhile;
            ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <?php
              if (have_posts()) :
                while (have_posts()) : the_post();
                  $storiesTitle = rwmb_meta('stories-title',$post->ID );
                  $storiesDesc = rwmb_meta('stories-desc',$post->ID );
                  $storiesVideoUri  = rwmb_meta('stories-url', $post->ID);

                  if ( $countOne == $wp_query->post_count):
                ?>
                    <div class="item active">
                      <iframe width="720" height="480" src="<?php echo $storiesVideoUri; ?>" frameborder="0" allowfullscreen="" class="center-block col-xs-12 col-sm-8"></iframe>
                      <div class="col-xs-12 col-sm-4 stories-info no-padding">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/Icono1.png" alt="" class="col-xs-2 col-sm-2 img-responsive stories-img left no-padding">
                        <p class="stories-title col-xs-9 col-sm-12 no-padding"><?php echo $storiesTitle; ?></p>
                        <p class="stories-desc col-xs-12 no-padding"><?php echo $storiesDesc; ?></p>
                      </div>
                    </div>
                <?php
                  else:
                ?>
                    <div class="item">
                      <iframe width="720" height="480" src="<?php echo $storiesVideoUri; ?>" frameborder="0" allowfullscreen="" class="center-block col-xs-12 col-sm-8"></iframe>
                      <div class="col-xs-12 col-sm-4 stories-info no-padding">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/Icono1.png" alt="" class="col-xs-2 col-sm-2 img-responsive stories-img left no-padding">
                        <p class="stories-title col-xs-9 col-sm-12 no-padding"><?php echo $storiesTitle; ?></p>
                        <p class="stories-desc col-xs-12 no-padding"><?php echo $storiesDesc; ?></p>
                    </div>
                    </div>
                <?php
                  endif;

                  $countOne--;
                endwhile;
              endif;
              ?>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>


      </div>
    </div>
  </section>


  <!--  Contact  -->
  <section id="contacto" class="col-xs-12 no-padding">
    <div class="col-xs-12 contact-ppal center-div">
      <div class="container">
        <?php
          $args = array(
            'post_type' => 'post_contact',
            'post_status' => 'publish',
            'orderby' => 'ID',
            'order' => 'ASC',
            'posts_per_page' => '1',
          );

          query_posts($args);
          if (have_posts()) :
            while (have_posts()) : the_post();
              $contactMessage = rwmb_meta('contact-message',$post->ID );
              $miamiName = rwmb_meta('contact-name-miami',$post->ID );
              $miamiPhone = rwmb_meta('contact-phone-miami',$post->ID );
              $ccsName = rwmb_meta('contact-name-ccs',$post->ID );
              $ccsPhone = rwmb_meta('contact-phone-ccs',$post->ID );
              $contactFb = rwmb_meta('contact-fb',$post->ID );
              $contactTw = rwmb_meta('contact-tw',$post->ID );
              $contactIn = rwmb_meta('contact-ins',$post->ID );
            endwhile;
          endif;
        ?>
        <!--    Contact Mobile    -->
        <div id="contactCarousel" class="carousel slide visible-xs hidden-sm hidden-md hidden-lg" data-ride="carousel">
          <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#contactCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#contactCarousel" data-slide-to="1"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
               <div class="item active">
                  <div class="col-xs-12 no-padding">
                    <p class="contact-message center-block text-center"><?php echo $contactMessage; ?></p>
                    <div class="col-xs-6 no-padding text-center">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/ubication-marker.png" alt="" class="img-responsive center-block">
                      <p class="text-city">MIAMI</p>
                      <p class="text-info"><?php echo $miamiName; ?></p>
                      <p class="text-info"><?php echo $miamiPhone; ?></p>
                    </div>
                    <div class="col-xs-6 no-padding text-center">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/ubication-marker.png" alt="" class="img-responsive center-block">
                      <p class="text-city">CARACAS</p>
                      <p class="text-info"><?php echo $ccsName; ?></p>
                      <p class="text-info"><?php echo $ccsPhone; ?></p>
                    </div>
                  </div>
                  <div class="col-xs-12 no-padding div-sm-icons">
                    <div class="col-xs-2 col-xs-offset-1 no-padding div-fb">
                      <a href="<?php echo $contactFb; ?>" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/fb-icon.png" alt="fb" class="img-responsive">
                      </a>
                    </div>
                    <div class="col-xs-2 no-padding">
                      <a href="<?php echo $contactTw; ?>" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/tw-icon.png" alt="tw" class="img-responsive">
                      </a>
                    </div>
                    <div class="col-xs-2 no-padding">
                      <a href="<?php echo $contactIn; ?>" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ins-icon.png" alt="ins" class="img-responsive">
                      </a>
                    </div>
                  </div>
                </div>

                <div class="item">
                  <div class="col-xs-12 no-padding">
                    <?php
                      if ($lang == "en") {
                        echo do_shortcode( '[contact-form-7 id="58" title="Contact form (En)"]');
                      }  else {
                        echo do_shortcode( '[contact-form-7 id="237" title="Contact form (Es)"]');
                      }
                    ?>
                  </div>
                </div>
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#contactCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#contactCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>
        <!--    End Contact Mobile    -->
        <!-- Contact Desktop -->
        <div class="col-xs-12 col-sm-6 no-padding div-right-border hidden-xs visible-sm visible-md visible-lg">
          <?php
            if ($lang == "en") {
              echo do_shortcode( '[contact-form-7 id="58" title="Contact form (En)"]');
            }  else {
              echo do_shortcode( '[contact-form-7 id="237" title="Contact form (Es)"]');
            }
          ?>
        </div>
        <div class="col-xs-12 col-sm-6 no-padding hidden-xs visible-sm visible-md visible-lg">
          <p class="contact-message center-block text-center"><?php echo $contactMessage; ?></p>
          <div class="col-xs-12 no-padding">
            <div class="col-xs-12 col-sm-6 no-padding text-center">
              <img src="<?php echo get_template_directory_uri(); ?>/images/ubication-marker.png" alt="" class="img-responsive center-block">
              <p class="text-city">MIAMI</p>
              <p class="text-info"><?php echo $miamiName; ?></p>
              <p class="text-info"><?php echo $miamiPhone; ?></p>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding text-center">
              <img src="<?php echo get_template_directory_uri(); ?>/images/ubication-marker.png" alt="" class="img-responsive center-block">
              <p class="text-city">CARACAS</p>
              <p class="text-info"><?php echo $ccsName; ?></p>
              <p class="text-info"><?php echo $ccsPhone; ?></p>
            </div>
            <div class="col-xs-12 no-padding div-sm-icons">
              <div class="col-xs-1 col-xs-offset-0 col-sm-offset-4 no-padding div-fb">
                <a href="<?php echo $contactFb; ?>" target="_blank">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/fb-icon.png" alt="fb" class="img-responsive">
                </a>
              </div>
              <div class="col-xs-1 no-padding">
                <a href="<?php echo $contactTw; ?>" target="_blank">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/tw-icon.png" alt="tw" class="img-responsive">
                </a>
              </div>
              <div class="col-xs-1 no-padding">
                <a href="<?php echo $contactIn; ?>" target="_blank">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/ins-icon.png" alt="ins" class="img-responsive">
                </a>
              </div>
            </div>
          </div>
        </div>
        <!-- End Contact Desktop -->
        <!--    Publicidad    -->
        <div class="col-xs-12 text-center footer-txt-cp-1">
          <a href="http://msmtdev.com/" target="_blank">
             <span class="footer-txt-cp">by</span>
          </a>
        </div>
        <!--    Edn Publicidad    -->
      </div>
    </div>
  </section>
</div>

</div>
<!-- End Testimonial Section -->
<?php get_footer(); ?>